
import time
from cloudservices.queueservice.queue import Queue
from cloudservices.storageservice.storage import Storage


def main():

    queue = Queue(topic_name='')
    queue.client.send_messages([{"body": "test message"} for i in range(10)])

    storage = Storage(bucket_name='')
    storage.upload("test.txt", "test.txt")


if __name__ == '__main__':
    main()
